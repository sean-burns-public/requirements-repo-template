import os
import shutil
import sys
import datetime
from _helper import get_requirements_subfolders

REQUIREMENTS_TOOL_NAME = 'rexa' # Set this to the name of the requirements tool (i.e. the repository/folder name in which the tool is located).

backup_directory_created = False

# Find the absolute path to the 'rexa' folder that resides in the same folder that this module/file is within.
fileDir = os.path.dirname(os.path.abspath(__file__))
absRexaPath = os.path.join(fileDir, REQUIREMENTS_TOOL_NAME)

# If the requirements tool is not already on the system path, add it as the first path for Python to search for modules.
try:
    sys.path.index(absRexaPath)
except ValueError:
    sys.path.insert(0, absRexaPath)

# Ensure the current working directory is that of this file.
os.chdir(fileDir)

def suspend_termination():
    print("Unexpected error:", sys.exc_info()[0])
    import traceback
    traceback.print_exc()
    input("Press Enter to continue...")
    sys.exit()

# Import the main() function from the requirements tool. Note: The rexa folder must have been prepended to the system path for this to work.
from doorstop.cli.main import main

# Import all of the requirements' spreadsheets into rexa.
requirements_documents = get_requirements_subfolders('.')
for document in requirements_documents:
    spreadsheet_filename = document.lower() + '.xlsx'
    spreadsheet_path = './' + spreadsheet_filename
    if os.path.exists(spreadsheet_path):

        # Import the document's source spreadsheet.
        try:
            main(['import', spreadsheet_path, document.upper()])
        except:
            suspend_termination()

        # If not already done so, create a backup directory for all imported spreadsheets.
        if backup_directory_created == False:
            backup_dir = os.path.join("backup/", datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
            if not os.path.exists(backup_dir):
                os.makedirs(backup_dir)
            backup_directory_created = True

        # Move the spreadsheet to a backup folder in case the import is erroneous.
        os.rename(spreadsheet_path, backup_dir + "/" + spreadsheet_filename)

# Publish the requirements documents in HTML and the requirements tree (via Mermaid).
try:
    main(['publish', 'all', './dist/'])
    main(['publish', '-M', 'all', './dist/'])
except:
    suspend_termination()

# Copy all the images which need to be shown within the published HTML documents.
shutil.rmtree('dist\images', ignore_errors=True)
shutil.copytree('images', 'dist\images')

input("Press Enter to continue...")
