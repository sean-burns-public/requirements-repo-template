import os

def get_requirements_subfolders(path):
    subfolders = []
    immediate_subfolders = next(os.walk(path))[1]
    for immediate_subfolder in immediate_subfolders:
        immediate_subfolder_entries = os.listdir(path + '/' + immediate_subfolder)
        if '.doorstop.yml' in immediate_subfolder_entries:
            subfolders.append(immediate_subfolder)
            subfolders.extend(get_requirements_subfolders(path + '/' + immediate_subfolder))
    return subfolders
