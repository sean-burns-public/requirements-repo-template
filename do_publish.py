import os
import shutil
import sys

REQUIREMENTS_TOOL_NAME = 'rexa' # Set this to the name of the requirements tool (i.e. the repository/folder name in which the tool is located).

# Find the absolute path to the 'rexa' folder that resides in the same folder that this module/file is within.
fileDir = os.path.dirname(os.path.abspath(__file__))
absRexaPath = os.path.join(fileDir, REQUIREMENTS_TOOL_NAME)

# If the requirements tool is not already on the system path, add it as the first path for Python to search for modules.
try:
    sys.path.index(absRexaPath)
except ValueError:
    sys.path.insert(0, absRexaPath)

# Ensure the current working directory is that of this file.
os.chdir(fileDir)

# Delete any existing published documents.
shutil.rmtree('dist', ignore_errors=True)

def suspend_termination():
    print("Unexpected error:", sys.exc_info()[0])
    import traceback
    traceback.print_exc()
    input("Press Enter to continue...")
    sys.exit()

# Import the main() function from the requirements tool. Note: The rexa folder must have been prepended to the system path for this to work.
from doorstop.cli.main import main

# Publish the requirements documents in HTML and the requirements tree (via Mermaid).
try:
    main(['publish', 'all', './dist/'])
    main(['publish', '-M', 'all', './dist/'])
except:
    suspend_termination()

# Copy all the images which need to be shown within the published HTML documents.
shutil.copytree('images', 'dist\images')

input("Press Enter to continue...")
