import os
import shutil
import sys
from _helper import get_requirements_subfolders

REQUIREMENTS_TOOL_NAME = 'rexa' # Set this to the name of the requirements tool (i.e. the repository/folder name in which the tool is located).

# Find the absolute path to the 'rexa' folder that resides in the same folder that this module/file is within.
fileDir = os.path.dirname(os.path.abspath(__file__))
absRexaPath = os.path.join(fileDir, REQUIREMENTS_TOOL_NAME)

# If the requirements tool is not already on the system path, add it as the first path for Python to search for modules.
try:
    sys.path.index(absRexaPath)
except ValueError:
    sys.path.insert(0, absRexaPath)

# Ensure the current working directory is that of this file.
os.chdir(fileDir)

failed_spreadsheet_exports = []

requirements_documents = get_requirements_subfolders('.')

def suspend_termination():
    print("Unexpected error:", sys.exc_info()[0])
    import traceback
    traceback.print_exc()
    input("Press Enter to continue...")
    sys.exit()

# Import the main() function from the requirements tool. Note: The rexa folder must have been prepended to the system path for this to work.
from doorstop.cli.main import main

for document in requirements_documents:
    spreadsheet_path = './' + document.lower() + '.xlsx'
    if os.path.exists(spreadsheet_path):
        failed_spreadsheet_exports.append(spreadsheet_path)
    else:
        try:
            main(['export', '-x', document.upper(), spreadsheet_path])
        except:
            suspend_termination()

if len(failed_spreadsheet_exports) > 0:
    print("The following requirements spreadsheets already existed and so have not been exported:")
    for failed_spreadsheet in failed_spreadsheet_exports:
        print("\t" + failed_spreadsheet)

input("Press Enter to continue...")
