# XXXX Requirements

## Prerequisites

- Python 3.8+
- Git (needed to run `do_import.py`)


## Initial Setup (Windows)

In order to work with this requirements project you will need to install Rexa, as follows (we won't actually use the installed copy of Rexa, but installing it will ensure all dependencies are installed):

1. Open Windows Command Prompt as Administrator, change the working directory to the root ot this repository and then execute the following command:

```
pip install --user ./rexa
```

## Publish the Existing Requirements

The following instructions detail how to publish the existing requirements such that they become local HTML files within the `dist` folder. Note that separate instructions detail how to export & import to/from spreadsheets; during the spreadhseet importation process, the requirements are automatically published.

1. Execute the `do_publish.py` file that is in the root of the repository.

1. Open the `dist` folder.

1. Either open the `.html` file for the requirements document/level of interest or open `index.html` to see a hierarchical tree.

## Exporting & Importing the Requirements to/from Spreadsheets

1. Execute the `do_export.py` file that is in the root of the repository.

1. One `.xlsx` file will be created in the root of the repository for each document/level of requirements.

1. Edit the `.xlsx` files as required, save the changes and close the files.

1. Execute the `do_import.py` file that is in the root of the repository.

## Working with the Requirements in the Spreadsheets

Once the requirements have been exported to spreadsheets, use the following guidelines whilst editing the data:

- The requirements themselves are written in the `text` column.
- The `uid` column contains a unique index for the requirement. It must be in the format given by prior examples, with a prefix, a dash and then a 3-digit number that is unique to a particular requirements document, for example: `USER-007`.
- Often multiple paragraphs need to be entered in the various fields; in such cases use <Alt> + <Enter>.
- Rows/entries that have the `normative` field set to `FALSE` are not requirements but they will need a unique `uid` of the correct format. They are headings and text for documentation purposes.
